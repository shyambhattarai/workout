package com.hfad.workout;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class WorkoutListFragment extends ListFragment
{
    public WorkoutListFragment()
    {
        // Required empty public constructor
    }

    static interface Listener
    {
        void itemClicked(long id);
    }

    private Listener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        int countOfWorkouts = Workout.workouts.length;
        String[] workoutArray = new String[countOfWorkouts];

        for(int count = 0 ; count < countOfWorkouts; count++ )
        {
            workoutArray[count] = Workout.workouts[count].getName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(inflater.getContext(),
                android.R.layout.simple_list_item_1, workoutArray);
        setListAdapter(adapter);

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        this.listener = (Listener) context;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        if(listener != null)
            listener.itemClicked(id);
    }
}
