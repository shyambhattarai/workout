package com.hfad.workout;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutDetailFragment extends Fragment
{

    private long workoutID;

    public WorkoutDetailFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
        {
            StopwatchFragment stopwatchFragment = new StopwatchFragment();
            FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.stopwatch_container, stopwatchFragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.commit();
        }
        else
            setWorkoutID(savedInstanceState.getLong("savedWorkoutID"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout_detail, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        View view = getView();

        TextView title = view.findViewById(R.id.textTitle);
        TextView description = view.findViewById(R.id.textDescription);

        Workout workout = Workout.workouts[(int) getWorkoutID()];
        title.setText(workout.getName());
        description.setText(workout.getDescription());
    }

    public void setWorkoutID(long id)
    {
        this.workoutID = id;
    }

    public long getWorkoutID()
    {
        return workoutID;
    }

    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putLong("savedWorkoutID", workoutID);
    }

}
