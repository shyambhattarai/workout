package com.hfad.workout;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class StopwatchFragment extends Fragment implements View.OnClickListener
{
    public StopwatchFragment()
    {
        // Required empty public constructor
    }

    private int seconds = 0;
    private boolean running = false;
    private boolean previouslyRunning = false;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null)
        {
            seconds = savedInstanceState.getInt("seconds");
            running = savedInstanceState.getBoolean("running");
            previouslyRunning = savedInstanceState.getBoolean("previouslyRunning");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_stopwatch, container, false);
        runTimer(layout);

        Button startBtn = (Button) layout.findViewById(R.id.startBtn);
        startBtn.setOnClickListener(this);

        Button stopBtn = (Button) layout.findViewById(R.id.stopBtn);
        stopBtn.setOnClickListener(this);

        Button resetBtn = (Button) layout.findViewById(R.id.resetBtn);
        resetBtn.setOnClickListener(this);

        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putInt("seconds", seconds);
        savedInstanceState.putBoolean("running", running);
        savedInstanceState.putBoolean("previouslyRunning", previouslyRunning);
    }

    //region Stopwatch Buttons Functionality

    //Start the stopwatch when the start button is clicked
    private void onClickStart()
    {
        running = true;
    }

    //Stop the stopwatch when the stop button is clicked
    private void onClickStop()
    {
        running = false;
    }

    //Reset the stopwatch when the reset button is clicked
    private void onClickReset()
    {
        running = false;
        seconds = 0;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.startBtn:
                onClickStart();
                break;

            case R.id.stopBtn:
                onClickStop();
                break;

            case R.id.resetBtn:
                onClickReset();
                break;
        }
    }

    //endregion


    //region Lifecycle Methods

    @Override
    public void onPause()
    {
        super.onPause();
        previouslyRunning = running;
        running = false;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(previouslyRunning)
            running = true;
    }

    //endregion

    //Function loops around after each second (1000 ms)
    private void runTimer(View view)
    {
        final TextView timeView = view.findViewById(R.id.stopwatchDisplay);
        final Handler handler = new Handler();

        handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                int hours = seconds/3600;
                int minutes = (seconds%3600)/60;
                int secs = seconds%60;

                String time = String.format(Locale.getDefault(),
                        "%d:%02d:%02d", hours, minutes, secs);

                timeView.setText(time);

                if(running)
                {
                    seconds++;
                }

                handler.postDelayed(this, 1000);
            }
        });
    }
}
